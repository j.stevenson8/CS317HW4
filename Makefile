CCOMP = gcc
COMPILEFLAGS = 

prog: main.o FA.o Stack.o header.h
	$(CCOMP) $(COMPILEFLAGS) -o prog main.o FA.o Stack.o

main.o: main.c FA.c header.h
	$(CCOMP) $(COMPILEFLAGS) -c main.c FA.c Stack.c

FA.0: FA.c header.h
	$(CCOMP) $(COMPILEFLAGS) -c FA.c

Stack.o: Stack.c FA.c header.h
	$(CCOMP) $(COMPILEFLAGS) -c Stack.c FA.c

clean:
	rm *.o prog
