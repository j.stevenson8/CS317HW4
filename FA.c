#include "header.h"

/* newTran creates a new Transition. a Transition includes 
 * a state number, an input character, a transition state number,
 * an isStart value, an isAccept value, and a pointer to antoher
 * Transition. The function returns the pointer to the Transition.
 */
TRANS newTran(TRANS tran, int state, char input, int nextTran, TRANS next, bool isStart, bool isAccept){
	tran = malloc(sizeof(struct Transition));//allocate memory to the Transition*
	tran->state = state;
	tran->input = input;
	tran->tranState = nextTran;
	tran->isStart = isStart;
	tran->isAccept = isAccept;
	tran->next = next;

	return tran;
}

/* indexChar is used to index input characters.
 * It returns the index corresponding to the 
 * given input character.
 */
int indexChar(char symbol){
	switch (symbol){
		case 'a':
			return 0;
		case 'b':
			return 1;
		case 'c':
			return 2;
		case 'd':
			return 3;
		case 'e':
			return 4;
		default :
			return -1;//if the character was not recognized, return -1
	}
}

/* printTransitionList prints out an NFAs transition list.
 * It is given the pointer to the first state in the NFA and 
 * goes through the transitions until there are no more
 */
void printTransitionList(struct Transition* list){
	if(list == NULL){//if there are no more transitions, return
		return;
	}
	
	if(list->isStart == true){//if the state in this transition is a start state, print out its identifier
		printf("S");
	}
	else if(list->isAccept == true){//if the state in this transition is an accept state, print out its identifier
		printf("F");
	}
	else{//if it is neither. print out a space to align the output
		printf(" ");
	}

	printf("(q%d,%c)", list->state,list->input);//print out the state and its input

	if(list->tranState == -1){//if their is no transition state, go to the next line
		printf("\n");
	}
	else{
		printf("->q%d\n", list->tranState);//if there is a transition state, print it out
	}

	printTransitionList(list->next);//try to print the next transition in the list

	return;
}

/* findStart returns the state number of the start state
 * of an NFA. It must be given the pointer of the first 
 * state in the NFA.
 */
int findStart(struct Transition* list){
	if(list->isStart){//if this state is the start state, return its state number 
		return list->state;
	}

	return findStart(list->next);//if not, call findStart on the next transition
}

/* findAccept returns the state number of the accept state
 * of an NFA. It must be given the pointer of the first
 * state in the NFA.
 */
int findAccept(struct Transition* list){
	if(list->isAccept){//if this state is the accept state, return its state number
		return list->state;
	}

	return findAccept(list->next);//if not, call findAccept on the next transition
}
