#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

struct Transition{
	int state;
	bool isStart;
	bool isAccept;
	char input;
	int tranState;
	struct Transition* next;
};

struct Stack{
	struct Transition* list;
	struct Stack* next;
};

typedef struct Stack* STACK;
typedef struct Transition* TRANS;

TRANS newTran(TRANS, int, char, int, TRANS, bool, bool);
int indexChar(char);
void printTransitionList(struct Transition*);
struct Stack* push(struct Stack* new, struct Stack* old, struct Transition* list);
int findStart(struct Transition* list);
int findAccept(struct Transition* list);
struct Transition* top(struct Stack* stack);


