#include "header.h"

/* push pushes an NFA onto the stack.
 * It returns the address of the new 
 * stack.
 */
struct Stack* push(struct Stack* new, struct Stack* old, struct Transition* list){
	new = malloc(sizeof(struct Stack));//allocate memory for the new stack pointer
	new->list = list;//set the top of the stack to be the new NFA
	new->next = old;//set the top of the stack to point to the old top of stack
	return new;//return the address of the new stack
}

/* top returns the NFA on the top of the stack.
 * Must be given a stack pointer.
 */
struct Transition* top(struct Stack* stack){
	struct Transition* temp = stack->list; 

	return temp;//return the NFA on the top of the stack
}
