#include "header.h"

/* The main function handles all I/O of the program and puts all the functions
 * of the program together. It must be passed an argument that is a test file
 * to be evaluated. The test file will not be edited, only read. It will evaluate
 * every line in the test file as a regular expression in postfix form and 
 * generate a finite automata that represents it. The operations main can perform
 * are AND, OR, and Kleene Star. The language of the regular expressions must be 
 * {a,b,c,d,e}. The program generates a FA for every element in the language and 
 * pushes them onto a stack. When the program reads an operation, it pops FAs from 
 * the stack, performs the operation on them, and pushes them back onto the stack.
 * Once the end of an expression is reached, the program prints out the resulting 
 * NFA transitions and begins processing the next expression. Expressions can be 
 * at most 255 characters long. Any error will end the program immediately after 
 * displaying an error message.     
 */ 
void main(int argc, char** argv){
	//***Check Program Arguments***
	if(argc >= 2){ //check to see if the user supplied a test file
		printf("Using %s as the input file\n", argv[1]);
	}
	else{
		printf("ERROR\nPlease specify an input file as a program argument\n");
		return;
	}
	//***Done Checking Program Arguments***

	printf("\n\n\n");

	//***Try to Open Input File***

	FILE* fp = fopen(argv[1], "r");//fp is the file pointer to the test file
	if(fp == NULL){ //exit program if the test file does not exist in the current directory
		printf("ERROR\nInvalid input file\nMake sure your desired input file is in the current directory\n");
		return;
	}
	
	printf("Successfully opened input file \"%s\"\n", argv[1]);
	//***Done Trying to Open Input File***
	
	printf("\n\n\n");

	//***Read From Input File***
	char input[256];//input string buffer
	
	//below are temporary variables used in the process of generating NFAs
	TRANS accept;
	TRANS start;
	TRANS start1;
	TRANS temp2;
	TRANS temp3;
	TRANS temp4;
	STACK temp = NULL;

	int startState;//used to temporarily store the state number of the start state of an NFA
	int acceptState;//used to temporarily store the state number of the accept state of an NFA

	while(fgets(input,256,fp) != NULL){//read through the entire file
		int lastState = 0;//initial value for lastState;
		STACK stack = NULL;//create stack structure
		input[strlen(input)-1] = '\0';//gets rid of newline at the end of each line of input

		//***Read From Input String***
		for( int i = 0; i < strlen(input); i++){
			switch (input[i]){//read each character and perform the operation associated with that character
				case '&':
					/*AND operation
					 * The AND operation is performed in the following manner:
					 * 	
					 * 	The top two NFAs are popped off the stack
					 * 	Create a transition from the accept state of the second state popped 
					 * 		to the start state of the first state popped
					 *	The new NFA is then pushed onto the stack
					 */

					accept = top(stack);//pop the top two elements off the stack
					stack = stack->next;
					start = top(stack);
					stack = stack->next;

					temp2 = start;
					startState = findStart(accept);//startState now holds the state number of the start state of the first NFA popped 
					while(!(temp2->isAccept)){//find the accept state of the second NFA popped
						temp2 = temp2->next;
					}
					temp2->isAccept = false;//set the accept state to false
					temp2->tranState = startState;//set the transition state of the second NFA popped to the start state of the first NFA popped 
					temp2->next = accept;//set the next pointer of the second NFA popped to the first NFA popped 
					
					temp2 = accept;
					while(!(temp2->isStart)){//find the start state of the first NFA popped
						temp2 = temp2->next;
					}
					temp2->isStart = false;//make it not a start state

					stack = push(temp,stack,start);//push the new NFA onto the stack			
					break;
				case '|': 
					/* OR operation
					 * The OR operation is performed in the following manner:
					 * 	The top two NFAs are popped off the stack
					 * 	Create a new start state with transitions going to 
					 * 		the start states of both NFAs
					 * 	Create a new accept state with transitions coming 
					 * 		from the accept states of both NFAs
					 * 	The new NFA is pushed onto the stack
					 */

					accept = top(stack);//pop the top two elements off the stack
					stack = stack->next;
					temp3 = top(stack);
					stack = stack->next;

					temp4 = newTran(start,lastState+2,'E',-1,NULL,false,true);//create a new accept state with no transitions
					

					startState = findStart(accept);//startState now holds the state number of the start state of the first state popped 
					start = newTran(start,lastState+1,'E',startState,temp4,true,false);//create a new start state that points to the start state of the first state popped
					startState = findStart(temp3);
					start1 = newTran(start1,lastState+1,'E',startState,start,true,false);//create a new transition from the new start state to the start state of the second state popped
					
					temp2 = accept;
					while(!(temp2->isStart)){//find the start state of the first NFA popped
						temp2 = temp2->next;
					}
					temp2->isStart = false;//make it not a start state
					
					temp2 = accept;
					while(!(temp2->isAccept)){//find the accept state of the first NFA popped
						temp2 = temp2->next;
					}
					temp2->next = start1;//string this transition to the new one created 
					temp2->isAccept = false;//make it not an accept state
					temp2->tranState = lastState + 2;//make an e-transition to the new accept state

					temp2 = temp3;
					while(!(temp2->isStart)){//same as above for the second state popped 
						temp2 = temp2->next;
					}
					temp2->isStart = false;
					
					temp2 = temp3;
					while(!(temp2->isAccept)){
						temp2 = temp2->next;
					}
					temp2->next = accept;
					temp2->isAccept = false;
					temp2->tranState = lastState + 2;

					lastState++;//increment the lastState counter	

					stack = push(temp,stack,temp3);//push the new NFA onto the stack			
							
					break;
				case '*':
					/* STAR operation
					 * The Kleene Star operation is performed in the following manner:
					 * 	The top NFA is popped off the stack
					 * 	Create a new accept state with a transition coming from the 
					 * 		accept state of the popped NFA
					 * 	Create a new transition from the accept state of the popped 
					 * 		NFA to its start state
					 * 	The new NFA is pushed onto the stack
					 */

					accept = top(stack);//pop the top NFA
					stack = stack->next;

					startState = findStart(accept);//startState now holds the state number of the start state of the popped NFA
					acceptState = findAccept(accept);//acceptState now holds the state number of the accept state of the popped NFA
					start = newTran(start,acceptState,'E',lastState+1,NULL,false,false);//create an e-transition from the accept state of the popped NFA to the new accept state 

					temp2 = accept;
					while(!(temp2->isAccept)){//find the accept state of the NFA
						temp2 = temp2->next;
					}
					temp2->tranState = startState;//set the old e-transition to point to the start state of the NFA
					temp2->next = start;//string this transition to the new transition
					temp2->isAccept = false;//make it not an accept state
					
					start = newTran(start,lastState+1,'E',-1,NULL,false,true);//make a new accept state
					temp2 = temp2->next;
					temp2->next = start;//string the new transition to the new accept state

					stack = push(temp,stack,accept);//push the new NFA onto the stack
					lastState++;

					break;
				default :
					/* create a new NFA
					 * At this point in the switch statement, the program assumes that this 
					 * is a symbol in the laguage and attempts to make an NFA for it. If this
					 * is not a valid symbol in the language, the program will terminate after
					 * displaying an error message. The new NFA is created by creating two 
					 * states: a start state and an accept state. The start state moves to the 
					 * accept state when the symbol is input. This NFA is then pushed onto the stack.
					 */

				       	accept = newTran(accept,lastState+2,'E',-1,NULL,false,true);//create a new accept state
					start = newTran(start,lastState+1,input[i],lastState+2,accept,true,false);//create a new start state
					lastState += 2;//increment the lastState counter
					stack = push(temp,stack,start);//push the new NFA onto the stack
					break;
			}
		}
		//***Done Reading From Input String***
	
		//***Print Out Transition List***
		printTransitionList(stack->list);
		//***Done Printing Out Transition List***

		printf("\n\n\n");
	
	}
	
	printf("\n\n\nEnd of file reached\n");
	
	//***Done Reading From Input File***
		
	printf("\n\n\n");

	


	return;
}	

